//
//  FZPeeringView.m
//  bracelateapp
//
//  Created by Jonathan Araujo-Levy on 03/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "FZPeeringView.h"

@implementation FZPeeringView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadUI];
    }
    return self;
}

- (void) loadUI {
    
    UIImage *peeringBGimage = [UIImage imageNamed:@"view-main-circle-sync.png"];
    UIImageView *peeringBGview = [[UIImageView alloc] init];
    
    [peeringBGview setImage:peeringBGimage];
    [peeringBGview setFrame:CGRectMake((self.frame.size.width/2)-(peeringBGimage.size.width/2),
                                          (self.frame.size.height/2)-(peeringBGimage.size.height/1.5),
                                          peeringBGimage.size.width, peeringBGimage.size.height)];
    
    [self addSubview:peeringBGview];
}


@end
