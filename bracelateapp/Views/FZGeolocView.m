//
//  FZGeolocView.m
//  bracelateapp
//
//  Created by Jonathan Araujo-Levy on 03/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "FZGeolocView.h"

@implementation FZGeolocView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadUI];
    }
    return self;
}

- (void) loadUI {
    
    UIImage *geoBGimage = [UIImage imageNamed:@"view-main-circle-geo.png"];
    UIImageView *geoBGview = [[UIImageView alloc] init];
    
    [geoBGview setImage:geoBGimage];
    [geoBGview setFrame:CGRectMake((self.frame.size.width/2)-(geoBGimage.size.width/2),
                                       (self.frame.size.height/2)-(geoBGimage.size.height/1.5),
                                       geoBGimage.size.width, geoBGimage.size.height)];
    
    [self addSubview:geoBGview];
}


@end
