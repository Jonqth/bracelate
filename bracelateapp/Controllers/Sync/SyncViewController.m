//
//  SyncViewController.m
//  bracelateapp
//
//  Created by Aymeric Gallissot on 03/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "SyncViewController.h"

#import "FZPeering.h"

@interface SyncViewController ()

@end

@implementation SyncViewController

- (id)init{
    self = [super init];
    if (self) {
        
        // Custom initialization
        
        [self setLogo];
        [self setButtonBack];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIImageView *imageStatic = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 504)];
    [imageStatic setImage:[UIImage imageNamed:@"Sync-static.png"]];
    
    [self.view addSubview:imageStatic];
    [[FZPeering sharedBluetooth] sendData:@"Bienvenue sur Bracelate"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
