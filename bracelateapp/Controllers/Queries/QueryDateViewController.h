//
//  QueryDateViewController.h
//  bracelateapp
//
//  Created by Aymeric Gallissot on 03/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "FZViewController.h"

@interface QueryDateViewController : FZViewController

@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) NSDate *date;

@property (nonatomic, strong) UILabel *dateLabel;

@end
