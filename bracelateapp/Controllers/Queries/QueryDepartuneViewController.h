//
//  QueryDepartuneViewController.h
//  bracelateapp
//
//  Created by Aymeric Gallissot on 03/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "FZViewController.h"

@interface QueryDepartuneViewController : FZViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSArray *data;

@end
