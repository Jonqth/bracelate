//
//  QueryArrivalViewController.m
//  bracelateapp
//
//  Created by Aymeric Gallissot on 03/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "QueryArrivalViewController.h"

#import "FZQueries.h"

@interface QueryArrivalViewController ()

@end

@implementation QueryArrivalViewController

- (id)init{
    self = [super init];
    if (self) {
        
        [self setLogo];
        [self setButtonBack];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [titleLabel setText:@"Quelle destination"];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setFont:[UIFont fontWithName:@"Blanch-Caps" size:36.0]];
    [titleLabel setTextColor:[UIColor colorWithRed:3.0/255.0 green:138.0/255.0 blue:104.0/255.0 alpha:1.0]];
    [titleLabel setBackgroundColor:sharedAppDelegate.colorGreen];
    [self.view addSubview:titleLabel];
    
    self.departuneTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 120, 320, 72)];
    [self.departuneTextField setBackgroundColor:[UIColor clearColor]];
    [self.departuneTextField setFont:[UIFont fontWithName:@"Blanch-Caps" size:72.0]];
    [self.departuneTextField setTextAlignment:NSTextAlignmentCenter];
    [self.departuneTextField setTextColor:[UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1.0]];
    [self.departuneTextField setReturnKeyType:UIReturnKeyDone];
    [self.departuneTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.departuneTextField setText:@""];
    [self.departuneTextField setDelegate:self];
    [self.view addSubview:self.departuneTextField];
    
    // Focus
    [self.departuneTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [FZQueries sharedQueries].arrival = textField.text;
    [self.navigationController popViewControllerAnimated:YES];
    
    return NO;
}

@end
