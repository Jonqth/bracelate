//
//  QueryArrivalViewController.h
//  bracelateapp
//
//  Created by Aymeric Gallissot on 03/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "FZViewController.h"

@interface QueryArrivalViewController : FZViewController <UITextFieldDelegate>

@property (strong, nonatomic) UITextField *departuneTextField;
@end
