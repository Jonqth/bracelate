//
//  QueryDepartuneViewController.m
//  bracelateapp
//
//  Created by Aymeric Gallissot on 03/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "QueryDepartuneViewController.h"

#import "FZQueries.h"

@interface QueryDepartuneViewController ()

@end

@implementation QueryDepartuneViewController

- (id)init{
    self = [super init];
    if (self) {
        // Custom initialization
                
        [self setLogo];
        [self setButtonBack];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.data = [NSArray arrayWithObjects:@"Bourse", @"Quatre-Septembre", @"Sentier", @"Richelien-Drouot", @"Grands Boulevards", nil];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [titleLabel setText:@"D'où partez-vous"];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setFont:[UIFont fontWithName:@"Blanch-Caps" size:36.0]];
    [titleLabel setTextColor:[UIColor colorWithRed:3.0/255.0 green:138.0/255.0 blue:104.0/255.0 alpha:1.0]];
    [titleLabel setBackgroundColor:sharedAppDelegate.colorGreen];
    [self.view addSubview:titleLabel];
    
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 44, 320, 920)];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setScrollsToTop:NO];
    
    [self.view addSubview:self.tableView];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    
    
    // Cell selection
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    UIView *cellView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 46)];
    [cellView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Depart-cell.png"]]];
    
    
    UILabel *station = [[UILabel alloc] initWithFrame:CGRectMake(55, 0, 250, 44)];
    [station setBackgroundColor:[UIColor clearColor]];
    [station setFont:[UIFont fontWithName:@"Blanch-Caps" size:28.0]];
    [station setTextColor:[UIColor colorWithRed:172.0/255.0 green:172.0/255.0 blue:172.0/255.0 alpha:1.0]];
    [station setText:[self.data objectAtIndex:indexPath.row]];
    [cellView addSubview:station];
    
    
    [cell addSubview:cellView];
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 46;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [FZQueries sharedQueries].departune = [self.data objectAtIndex:indexPath.row];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
