//
//  QueryDateViewController.m
//  bracelateapp
//
//  Created by Aymeric Gallissot on 03/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "QueryDateViewController.h"

#import "FZQueries.h"

@interface QueryDateViewController ()

@end

@implementation QueryDateViewController

- (id)init{
    self = [super init];
    if (self) {
        
        [self setLogo];
        [self setButtonBack];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if([FZQueries sharedQueries].date){
        self.date = [FZQueries sharedQueries].date;
    }
    else{
        [FZQueries sharedQueries].date = [NSDate dateWithTimeIntervalSinceNow:60*5];
        self.date = [NSDate dateWithTimeIntervalSinceNow:60*5];
    }
	
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [titleLabel setText:@"Quand arrivez-vous"];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setFont:[UIFont fontWithName:@"Blanch-Caps" size:36.0]];
    [titleLabel setTextColor:[UIColor colorWithRed:3.0/255.0 green:138.0/255.0 blue:104.0/255.0 alpha:1.0]];
    [titleLabel setBackgroundColor:sharedAppDelegate.colorGreen];
    [self.view addSubview:titleLabel];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *date = [dateFormatter stringFromDate:self.date];
    
    self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 120, 320, 72)];
    [self.dateLabel setBackgroundColor:[UIColor clearColor]];
    [self.dateLabel setFont:[UIFont fontWithName:@"Blanch-Caps" size:72.0]];
    [self.dateLabel setTextAlignment:NSTextAlignmentCenter];
    [self.dateLabel setTextColor:[UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1.0]];
    [self.dateLabel setText:date];
    [self.view addSubview:self.dateLabel];
    
    // Date Picker
    self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 288, 325, 300)];
    self.datePicker.datePickerMode = UIDatePickerModeTime;
    self.datePicker.date = self.date;
    self.datePicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:60*5];
    
    [self.datePicker addTarget:self action:@selector(LabelChange:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.datePicker];
}

- (void)LabelChange:(id)sender{
    
    self.datePicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:60*5];
    self.date = self.datePicker.date;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *date = [dateFormatter stringFromDate:self.date];
    [self.dateLabel setText:date];
    
    [FZQueries sharedQueries].date = self.date;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
