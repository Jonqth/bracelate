//
//  MainViewController.m
//  bracelateapp
//
//  Created by Aymeric Gallissot on 02/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "MainViewController.h"
#import "FZPeering.h"
#import "FZPeeringView.h"
#import "SecondViewController.h"
//#import "FZGeolocView.h"

@interface MainViewController (){
    
    FZPeeringView *peeringView;
    //FZGeolocView *geolocView;
    
}

@end

@implementation MainViewController

- (id)init{
    self = [super init];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(peeringViewAnimation)
                                                     name:@"peeringNotification"
                                                   object:nil];
        [self setLogo];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadPeeringView];
}


// Peering Loading

- (void) loadPeeringView {
    // To view
    peeringView = [[FZPeeringView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    UIButton *peerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *peerButtonImage = [UIImage imageNamed:@"view-main-peer-icon.png"];

    [peerButton setFrame:CGRectMake((peeringView.frame.size.width/2)-(peerButtonImage.size.width/2),
                                  (peeringView.frame.size.height/2)-(peerButtonImage.size.height/1.6),
                                  peerButtonImage.size.width, peerButtonImage.size.height)];
    
    
    [peerButton setBackgroundImage:peerButtonImage forState:UIControlStateNormal];
    [peerButton addTarget:self action:@selector(loadPeering) forControlEvents:UIControlEventTouchUpInside];
    
    [peeringView addSubview:peerButton];
    [self.view addSubview:peeringView];
    
    [self loadPeering];
}

- (void) loadPeering {
    // To peer
    [[FZPeering sharedBluetooth] initPicker];
}

- (void) peeringViewAnimation {
    
    [UIView animateWithDuration:.5 animations:^{
        [peeringView setFrame:CGRectMake(-400, peeringView.frame.origin.y, peeringView.frame.size.width, peeringView.frame.size.height)];
    }completion:^(BOOL finished) {
        SecondViewController *toViewController = [[SecondViewController alloc] init];
        [self.navigationController pushViewController:toViewController animated:YES];
    }];

}

// Peering Loading


/*
// Position Loading

- (void) loadPositionView {
    // To view
    geolocView = [[FZGeolocView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    UIButton *geoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *geoButtonImage = [UIImage imageNamed:@"view-main-geo-icon.png"];
    
    [geoButton setFrame:CGRectMake((geolocView.frame.size.width/2)-(geoButtonImage.size.width/2),
                                    (geolocView.frame.size.height/2)-(geoButtonImage.size.height/1.6),
                                    geoButtonImage.size.width, geoButtonImage.size.height)];
    
    
    [geoButton setBackgroundImage:geoButtonImage forState:UIControlStateNormal];
    [geoButton addTarget:self action:@selector(loadPeering) forControlEvents:UIControlEventTouchUpInside];
    
    [geolocView setAlpha:0.f];
    
    [geolocView addSubview:geoButton];
    [self.view addSubview:geolocView];
    
    [UIView animateWithDuration:.5 animations:^{
        [geolocView setAlpha:0.f];
    }];
}

- (void) loadPosition {

}


// Position Loading
*/


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
