//
//  SecondViewController.h
//  bracelateapp
//
//  Created by Aymeric Gallissot on 03/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "FZViewController.h"

@interface SecondViewController : FZViewController

@property (strong, nonatomic) UIButton *searchButton;

@property (strong, nonatomic) UILabel *labelDepartune;
@property (strong, nonatomic) UILabel *labelArrival;
@property (strong, nonatomic) UILabel *labelDate;

@end
