//
//  SecondViewController.m
//  bracelateapp
//
//  Created by Aymeric Gallissot on 03/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "SecondViewController.h"

#import "QueryDepartuneViewController.h"
#import "QueryArrivalViewController.h"
#import "QueryDateViewController.h"

#import "SearchViewController.h"

#import "FZQueries.h"


@interface SecondViewController ()

@end

@implementation SecondViewController

- (id)init{
    self = [super init];
    if (self) {
        // Custom initialization
        
        [self setLogo];
        [self setButtonMenu];
        [self setButtonLocation];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    UILabel *title1 = [[UILabel alloc] initWithFrame:CGRectMake(44, 20, 320-88, 44)];
    [title1 setBackgroundColor:[UIColor clearColor]];
    [title1 setFont:[UIFont fontWithName:@"Blanch-Caps" size:32.0]];
    [title1 setTextColor:[UIColor colorWithRed:16.0/255.0 green:16.0/255.0 blue:16.0/255.0 alpha:1.0]];
    [title1 setText:@"Départ"];
    [self.view addSubview:title1];
    
    UILabel *title2 = [[UILabel alloc] initWithFrame:CGRectMake(44, 118, 320-88, 44)];
    [title2 setBackgroundColor:[UIColor clearColor]];
    [title2 setFont:[UIFont fontWithName:@"Blanch-Caps" size:32.0]];
    [title2 setTextColor:[UIColor colorWithRed:16.0/255.0 green:16.0/255.0 blue:16.0/255.0 alpha:1.0]];
    [title2 setText:@"Arrivée"];
    [self.view addSubview:title2];
    
    UILabel *title3 = [[UILabel alloc] initWithFrame:CGRectMake(44, 220, 320-88, 44)];
    [title3 setBackgroundColor:[UIColor clearColor]];
    [title3 setFont:[UIFont fontWithName:@"Blanch-Caps" size:32.0]];
    [title3 setTextColor:[UIColor colorWithRed:16.0/255.0 green:16.0/255.0 blue:16.0/255.0 alpha:1.0]];
    [title3 setText:@"Date d'arrivée"];
    [self.view addSubview:title3];
    
    // VIEW
    UIView *titleView1 = [[UIView alloc] initWithFrame:CGRectMake(44, 57, 232, 44)];
    [titleView1 setBackgroundColor:[UIColor colorWithRed:31.0/255.0 green:31.0/255.0 blue:31.0/255.0 alpha:1.0]];
    [self.view addSubview:titleView1];
    
    UIView *titleView2 = [[UIView alloc] initWithFrame:CGRectMake(44, 155, 232, 44)];
    [titleView2 setBackgroundColor:[UIColor colorWithRed:31.0/255.0 green:31.0/255.0 blue:31.0/255.0 alpha:1.0]];
    [self.view addSubview:titleView2];
    
    UIView *titleView3 = [[UIView alloc] initWithFrame:CGRectMake(44, 256, 232, 44)];
    [titleView3 setBackgroundColor:[UIColor colorWithRed:31.0/255.0 green:31.0/255.0 blue:31.0/255.0 alpha:1.0]];
    [self.view addSubview:titleView3];
    
    // VIEW Image
    UIImageView *arrow1 = [[UIImageView alloc] initWithFrame:CGRectMake(188, 0, 44, 44)];
    [arrow1 setImage:[UIImage imageNamed:@"Second-query.png"]];
    UIImageView *arrow2 = [[UIImageView alloc] initWithFrame:CGRectMake(188, 0, 44, 44)];
    [arrow2 setImage:[UIImage imageNamed:@"Second-query.png"]];
    UIImageView *arrow3 = [[UIImageView alloc] initWithFrame:CGRectMake(188, 0, 44, 44)];
    [arrow3 setImage:[UIImage imageNamed:@"Second-query.png"]];
    
    
    [titleView1 addSubview:arrow1];
    [titleView2 addSubview:arrow2];
    [titleView3 addSubview:arrow3];
    
    
    self.labelDepartune = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 168, 44)];
    [self.labelDepartune setBackgroundColor:[UIColor clearColor]];
    [self.labelDepartune setFont:[UIFont fontWithName:@"Blanch-Caps" size:28.0]];
    [self.labelDepartune setTextColor:[UIColor colorWithRed:172.0/255.0 green:172.0/255.0 blue:172.0/255.0 alpha:1.0]];
    [self.labelDepartune setText:@"..."];
    [titleView1 addSubview:self.labelDepartune];
    
    self.labelArrival = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 168, 44)];
    [self.labelArrival setBackgroundColor:[UIColor clearColor]];
    [self.labelArrival setFont:[UIFont fontWithName:@"Blanch-Caps" size:28.0]];
    [self.labelArrival setTextColor:[UIColor colorWithRed:172.0/255.0 green:172.0/255.0 blue:172.0/255.0 alpha:1.0]];
    [self.labelArrival setText:@"..."];
    [titleView2 addSubview:self.labelArrival];
    
    self.labelDate = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 168, 44)];
    [self.labelDate setBackgroundColor:[UIColor clearColor]];
    [self.labelDate setFont:[UIFont fontWithName:@"Blanch-Caps" size:28.0]];
    [self.labelDate setTextColor:[UIColor colorWithRed:172.0/255.0 green:172.0/255.0 blue:172.0/255.0 alpha:1.0]];
    [self.labelDate setText:@"..."];
    [titleView3 addSubview:self.labelDate];
    
    
    // Button
    UIButton *button1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 232, 44)];
    [button1 addTarget:self action:@selector(actionDepartune) forControlEvents:UIControlEventTouchUpInside];
    [titleView1 addSubview:button1];
    
    UIButton *button2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 232, 44)];
    [button2 addTarget:self action:@selector(actionArrival) forControlEvents:UIControlEventTouchUpInside];
    [titleView2 addSubview:button2];
    
    UIButton *button3 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 232, 44)];
    [button3 addTarget:self action:@selector(actionDate) forControlEvents:UIControlEventTouchUpInside];
    [titleView3 addSubview:button3];
    
    
    self.searchButton = [[UIButton alloc] initWithFrame:CGRectMake((320/2)-67, 340, 135, 135)];
    [self.searchButton setImage:[UIImage imageNamed:@"Second-search-button.png"] forState:UIControlStateNormal];
    [self.searchButton addTarget:self action:@selector(actionButtonSeach) forControlEvents:UIControlEventTouchUpInside];
    [self.searchButton setEnabled:NO];
    [self.view addSubview:self.searchButton];
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    if([FZQueries sharedQueries].departune && [FZQueries sharedQueries].arrival && [FZQueries sharedQueries].date){
        self.searchButton.enabled = YES;
    }
    else{
        self.searchButton.enabled = NO;
    }
    
    if([FZQueries sharedQueries].departune){
        [self.labelDepartune setText:[FZQueries sharedQueries].departune];
    }
    
    if([FZQueries sharedQueries].arrival){
        [self.labelArrival setText:[FZQueries sharedQueries].arrival];
    }
    
    if([FZQueries sharedQueries].date){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm"];
        NSString *date = [dateFormatter stringFromDate:[FZQueries sharedQueries].date];
        
        [self.labelDate setText:date];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)actionDepartune{
    QueryDepartuneViewController *viewDepartune = [[QueryDepartuneViewController alloc] init];
    [self.navigationController pushViewController:viewDepartune animated:YES];
}

- (void)actionArrival{
    QueryArrivalViewController *viewArrival = [[QueryArrivalViewController alloc] init];
    [self.navigationController pushViewController:viewArrival animated:YES];
}

- (void)actionDate{
    QueryDateViewController *viewDate = [[QueryDateViewController alloc] init];
    [self.navigationController pushViewController:viewDate animated:YES];
}

- (void)actionButtonSeach{
    
    SearchViewController *searchView = [[SearchViewController alloc] initWithStart:@"Bourse" andDestination:@"Nation" andArrivalTime:1359882656];
    [self.navigationController pushViewController:searchView animated:YES];
    
}

@end
