//
//  SearchViewController.h
//  bracelateapp
//
//  Created by Aymeric Gallissot on 03/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "FZViewController.h"

@interface SearchViewController : FZViewController <UIScrollViewDelegate>

@property (nonatomic, retain) UIActivityIndicatorView *activityIndicator;

- (id) initWithStart:(NSString *)start andDestination:(NSString *)destination andArrivalTime:(int)arrivalTime;

@end
