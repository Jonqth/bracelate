//
//  SearchViewController.m
//  bracelateapp
//
//  Created by Aymeric Gallissot on 03/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "SearchViewController.h"
#import "AFNetworking.h"

#import "SyncViewController.h"

@interface SearchViewController () {
    NSArray *dataArray;
    NSString *dataURL;
    UILabel *mylabel;
    UIScrollView *scrollView;
}

@end

@implementation SearchViewController

- (id)init {
    self = [super init];
    if (self) {
        [self setLogo];
        [self setButtonBack];
    }
    return self;
}

- (id) initWithStart:(NSString *)start andDestination:(NSString *)destination andArrivalTime:(int)arrivalTime {
    
    dataURL = [NSString stringWithFormat:@"http://192.168.2.87:8080?from=%@&to=%@&date=%d&walkspeed=good",start,destination,arrivalTime];
    
    self = [super init];
    if (self) {
        [self setLogo];
        [self setButtonBack];
        [self setNetworkingData];
    }
    return self;
}

- (void) setNetworkingData {
    
    dataArray = [[NSArray alloc] init];
    NSURL *url = [[NSURL alloc] initWithString:dataURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        dataArray = JSON;
        [self UIloading];
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
    }];
    
    // operation
    [operation start];

    // loader 
    [self dataLoader];
}

- (void) dataLoader {
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(150, 242, 20, 20)];
    [self.activityIndicator setBackgroundColor:[UIColor clearColor]];
    [self.activityIndicator setHidesWhenStopped:YES];
    [self.activityIndicator startAnimating];
    
    [self.view addSubview:self.activityIndicator];
}


- (void) UIloading {
    
    // remove loader
    [self.activityIndicator startAnimating];
    
    NSDictionary *jsondict = [[NSDictionary alloc] init];
    jsondict = [dataArray objectAtIndex:0];
    
    NSArray *steps = [[NSArray alloc] init];
    steps = [jsondict objectForKey:@"steps"];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    [scrollView setDelegate:self];
    
    for (int i = 0; i < dataArray.count; i++) {
        
        CGRect frame;
        
        if(i == 0){
            frame = CGRectMake((self.view.frame.size.width/2)-116, 30, 233, 120);
        }
        else if(i == 1){
            frame = CGRectMake((self.view.frame.size.width/2)-116, 156*i+40, 233, 120);
        }
        else if(i == 2){
            frame = CGRectMake((self.view.frame.size.width/2)-116, 156*i+50, 233, 120);
        }
        else{
            frame = CGRectMake((self.view.frame.size.width/2)-116, 156*i+20, 233, 120);
        }
        
        
        UIView *containerView = [[UIView alloc] initWithFrame:frame];
        
        UILabel *TitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, containerView.frame.size.width, 20)];
        [TitleLabel setBackgroundColor:[UIColor clearColor]];
        [TitleLabel setFont:[UIFont fontWithName:@"Blanch-Caps" size:28.0]];
        [TitleLabel setTextColor:[UIColor colorWithRed:23.0/255.0 green:23.0/255.0 blue:23.0/255.0 alpha:1.0]];
        //[TitleLabel setText:@"Départ 19:30 > Arrivée 21:19"];
        
        // Info
        UIView *infoView = [[UIView alloc] initWithFrame:CGRectMake(0, (TitleLabel.frame.origin.y)+(TitleLabel.frame.size.height+4), 233, 110)];
        [infoView setBackgroundColor:[UIColor colorWithRed:31.0/255.0 green:31.0/255.0 blue:31.0/255.0 alpha:1.0]];
        
        UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 233, 110-55)];
        [infoLabel setTextAlignment:NSTextAlignmentCenter];
        [infoLabel setBackgroundColor:[UIColor clearColor]];
        [infoLabel setFont:[UIFont fontWithName:@"Blanch-Caps" size:28.0]];
        [infoLabel setTextColor:[UIColor colorWithRed:172.0/255.0 green:172.0/255.0 blue:172.0/255.0 alpha:1.0]];
        [infoView addSubview:infoLabel];
        
        UIView *hourView = [[UIView alloc] initWithFrame:CGRectMake(0, 55, 233, 55)];
        [hourView setBackgroundColor:[UIColor colorWithRed:0/255.0 green:204/255.0 blue:153/255.0 alpha:1.0]];
    
        UILabel *hourLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 233, 55)];
        [hourLabel setTextAlignment:NSTextAlignmentCenter];
        [hourLabel setBackgroundColor:[UIColor clearColor]];
        [hourLabel setFont:[UIFont fontWithName:@"Blanch-Caps" size:40.0]];
        [hourLabel setTextColor:[UIColor colorWithRed:3.0/255.0 green:138.0/255.0 blue:104.0/255.0 alpha:1.0]];
        [hourView addSubview:hourLabel];
        
        //Title Data
        NSDictionary *jsondict = [[NSDictionary alloc] init];
            jsondict = [dataArray objectAtIndex:i];
            //NSLog(@" jsondict : %@", [dataArray objectAtIndex:i]);
        NSArray *steps = [[NSArray alloc] init];
            steps = [jsondict objectForKey:@"steps"];
            //NSLog(@" steps : %@", [jsondict objectForKey:@"steps"]);
        NSDictionary *final = [[NSDictionary alloc] init];
            final = [steps objectAtIndex:0];
            //NSLog(@" final : %@", [steps objectAtIndex:0]);
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm"];
        
        NSString *dateStartString = nil;
        NSString *dateEndString = nil;
        
        NSDate *dateStart = nil;
        NSDate *dateEnd = nil;
        
        if(i == 0){
            dateStart = [NSDate dateWithTimeIntervalSinceNow:60*15];
            dateEnd = [NSDate dateWithTimeIntervalSinceNow:60*33+60*15];
            dateStartString = [dateFormatter stringFromDate:dateStart];
            dateEndString = [dateFormatter stringFromDate:dateEnd];
        }
        else if(i == 1){
            dateStart = [NSDate dateWithTimeIntervalSinceNow:60*29];
            dateEnd = [NSDate dateWithTimeIntervalSinceNow:60*29+60*49];
            dateStartString = [dateFormatter stringFromDate:dateStart];
            dateEndString = [dateFormatter stringFromDate:dateEnd];
        }
        else if(i == 2){
            dateStart = [NSDate dateWithTimeIntervalSinceNow:60*56];
            dateEnd = [NSDate dateWithTimeIntervalSinceNow:60*56+60*25];
            dateStartString = [dateFormatter stringFromDate:dateStart];
            dateEndString = [dateFormatter stringFromDate:dateEnd];
        }
        
        int seconds = [dateEnd timeIntervalSince1970] - [dateStart timeIntervalSince1970];
        
        NSString *type = nil;
        
        if(i == 0){
            type = @"Arrivée au plus tôt";
        }
        else if(i == 1){
            type = @"moins de correspondance";
        }
        else if(i == 2){
            type = @"moins de marche à pied";
        }
        
        //[TitleLabel setText:[jsondict objectForKey:@"type"]];
        [TitleLabel setText:type];
        [infoLabel setText:[NSString stringWithFormat:@"Départ %@ - Arrivée %@",  dateStartString, dateEndString]];
        [hourLabel setText:[NSString stringWithFormat:@"%@", [self timeHour:seconds]]];
        
        [containerView addSubview:TitleLabel];
        [containerView addSubview:infoView];
        [infoView addSubview:hourView];
        [self.view addSubview:containerView];
        
        UIButton *touchTrain = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 320, 504)];
        [touchTrain addTarget:self action:@selector(actionSync) forControlEvents:UIControlEventTouchUpInside];
        [touchTrain setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:touchTrain];
    }
}


- (void)actionSync{
    SyncViewController *viewSync = [[SyncViewController alloc] init];
    [self.navigationController pushViewController:viewSync animated:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)timeHour:(int)seconds{
    NSString *string = nil;
    
    seconds = (int)(floor(seconds/60));
    
    string = [NSString stringWithFormat:@"%d MIN", seconds];
    
    return string;
}

@end
