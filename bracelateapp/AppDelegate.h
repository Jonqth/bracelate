//
//  AppDelegate.h
//  bracelateapp
//
//  Created by Aymeric Gallissot on 02/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UIColor *colorGreen;

@property (assign, nonatomic) BOOL isLocated;

@end
