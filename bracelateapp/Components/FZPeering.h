//
//  FZPeering.h
//  bracelateapp
//
//  Created by Jonathan Araujo-Levy on 03/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>

@interface FZPeering : NSObject <GKPeerPickerControllerDelegate, GKSessionDelegate>

@property (strong,nonatomic) GKPeerPickerController *FZPeerController;
@property (strong,nonatomic) GKSession *FZSession;

+ (FZPeering *) sharedBluetooth;

- (void) initPicker;
- (void) sendData:(NSString *)dataToSend;

@end
