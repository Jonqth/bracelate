//
//  FZQueries.m
//  bracelateapp
//
//  Created by Aymeric Gallissot on 03/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "FZQueries.h"

@implementation FZQueries

static FZQueries *sharedQueries = nil;

+ (FZQueries *)sharedQueries {
    
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        sharedQueries = [[FZQueries alloc] init];
    });
    
    return sharedQueries;
}

- (id)init{
    self = [super init];
    if (self) { }
    return self;
}

@end
