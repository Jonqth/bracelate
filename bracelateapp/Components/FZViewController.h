//
//  FZViewController.h
//  bracelateapp
//
//  Created by Aymeric Gallissot on 02/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface FZViewController : UIViewController <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *FZLocationManager;

- (void)setLogo;
- (void)setButtonBack;
- (void)setButtonMenu;
- (void)setButtonLocation;

- (void)actionButtonBack;
- (void)actionButtonMenu;
- (void)actionButtonLocation;

@end
