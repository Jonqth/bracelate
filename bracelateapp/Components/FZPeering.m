//
//  FZPeering.m
//  bracelateapp
//
//  Created by Jonathan Araujo-Levy on 03/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "FZPeering.h"

@interface FZPeering ()

@end

@implementation FZPeering

static FZPeering *sharedBluetooth = nil;

@synthesize FZPeerController;
@synthesize FZSession;

+ (FZPeering *) sharedBluetooth {
    
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        sharedBluetooth = [[FZPeering alloc] init];
    });
    
    return sharedBluetooth;
}

- (id)init{
    self = [super init];
    if (self) { }
    return self;
}

- (void) initPicker {
    if (self.FZSession == nil) {
        self.FZPeerController = [[GKPeerPickerController alloc] init];
        [self.FZPeerController setDelegate:self];
        [self.FZPeerController setConnectionTypesMask:GKPeerPickerConnectionTypeNearby];
        [self.FZPeerController show];
    }
}

// Sending data through bluetooth

- (void) sendData:(NSString *)dataToSend {
    NSData *textData = [dataToSend dataUsingEncoding:NSASCIIStringEncoding];
    [self.FZSession sendDataToAllPeers:textData withDataMode:GKSendDataReliable error:nil];
    NSLog(@"TEST");
}

////////////////////////////////////
// Peering Delegates


// Defining de Session

- (GKSession *)peerPickerController:(GKPeerPickerController *)picker sessionForConnectionType:(GKPeerPickerConnectionType)type {
    NSString *sessionIDString = @"BRACELATESESSION";
    self.FZSession = [[GKSession alloc] initWithSessionID:sessionIDString displayName:@"iPhone de Jonathan" sessionMode:GKSessionModePeer];
    return self.FZSession;
}

// Loading the session

- (void)session:(GKSession *)session peer:(NSString *)peerID didChangeState:(GKPeerConnectionState)state {
    if (state == GKPeerStateConnected){
        [session setDataReceiveHandler:self withContext:nil];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"peeringNotification"
         object:self];
    }
    else {
        [self.FZSession setDelegate:nil];
        self.FZSession = nil;
    }
}

// Connection suceeded

- (void)peerPickerController:(GKPeerPickerController *)picker didConnectPeer:(NSString *)peerID toSession:(GKSession *)session {
    session.delegate = self;
    self.FZSession = session;
    self.FZPeerController.delegate = nil;
    [self.FZPeerController dismiss];
}

- (void)peerPickerControllerDidCancel:(GKPeerPickerController *)picker {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"peeringNotification"
     object:self];
}

// Receiving Data

- (void)receiveData:(NSData *)data fromPeer:(NSString *)peer inSession:(GKSession *)session context:(void *)context {
    NSLog(@"RECEIVED");
}

@end
