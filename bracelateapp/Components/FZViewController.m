//
//  FZViewController.m
//  bracelateapp
//
//  Created by Aymeric Gallissot on 02/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "FZViewController.h"



@interface FZViewController () {
    NSString *reversePlacemark;
}

@end

@implementation FZViewController

@synthesize FZLocationManager;

- (id)init{
    self = [super init];
    if (self) {
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"Navbar.png"] forBarMetrics:UIBarMetricsDefault];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setShadowImage:[UIImage imageNamed:@"Navbar-shadow.png"]];
    [self.view setBackgroundColor:[UIColor colorWithRed:38.0/255.0 green:38.0/255.0 blue:38.0/255.0 alpha:1.0]];
    
    [self initLocationManager];
}

- (void) initLocationManager {
    if(!sharedAppDelegate.isLocated){
        self.FZLocationManager = [[CLLocationManager alloc] init];
        [self.FZLocationManager setDelegate:self];
        [self.FZLocationManager setDesiredAccuracy:kCLLocationAccuracyBest];
        [self.FZLocationManager setPausesLocationUpdatesAutomatically:YES];
        [self.FZLocationManager startUpdatingLocation];
        [self.FZLocationManager startUpdatingHeading];
        sharedAppDelegate.isLocated = YES;
        [self reverseGeocoding];
    }
}

// Reverse

- (NSString *) reverseGeocoding {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:self.FZLocationManager.location completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error){
            NSLog(@"Geocode failed with error: %@", error);
            return;
        }
        reversePlacemark = [placemarks objectAtIndex:0];
        NSLog(@"%@", reversePlacemark);
    }];
    return reversePlacemark;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setLogo {
    
    // Title
    UIImageView *titleView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,92,17)];
    [titleView setImage:[UIImage imageNamed:@"Navbar-logo.png"]];
    
    self.navigationItem.titleView = titleView;
}


// Button
- (void)setButtonBack{
    
    UIButton *buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonBack addTarget:self action:@selector(actionButtonBack) forControlEvents:UIControlEventTouchUpInside];
    [buttonBack setBackgroundImage:[UIImage imageNamed:@"Navbar-back.png"] forState:UIControlStateNormal];
    //[menuButton setBackgroundImage:[UIImage imageNamed:@"Navbar-back.png"] forState:UIControlStateHighlighted];
    
    const CGFloat BarButtonOffset = 0.0f;
    [buttonBack setFrame:CGRectMake(-5, 0, 44, 44)];
    
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(BarButtonOffset, 0, 44, 44)]; // no image
    [containerView addSubview:buttonBack];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:containerView];

    
    self.navigationItem.leftBarButtonItem = barButtonItem;
}

- (void)setButtonMenu{
    
    UIButton *buttonMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonMenu addTarget:self action:@selector(actionButtonMenu) forControlEvents:UIControlEventTouchUpInside];
    [buttonMenu setBackgroundImage:[UIImage imageNamed:@"Navbar-menu.png"] forState:UIControlStateNormal];
    //[menuButton setBackgroundImage:[UIImage imageNamed:@"Navbar-back.png"] forState:UIControlStateHighlighted];
    
    const CGFloat BarButtonOffset = 0.0f;
    [buttonMenu setFrame:CGRectMake(-5, 0, 44, 44)];
    
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(BarButtonOffset, 0, 44, 44)]; // no image
    [containerView addSubview:buttonMenu];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:containerView];
    
    
    self.navigationItem.leftBarButtonItem = barButtonItem;
}


- (void)setButtonLocation{
    
    UIButton *buttonLocation = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonLocation addTarget:self action:@selector(actionButtonLocation) forControlEvents:UIControlEventTouchUpInside];
    
    [buttonLocation setBackgroundImage:[UIImage imageNamed:@"Navbar-location.png"] forState:UIControlStateNormal];
    //[buttonMenu setBackgroundImage:[UIImage imageNamed:@"Navbar-menu.png"] forState:UIControlStateHighlighted];
    
    const CGFloat BarButtonOffset = 5.0f;
    [buttonLocation setFrame:CGRectMake(BarButtonOffset, 0, 44, 44)];
    
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(BarButtonOffset, 0, 44, 44)]; // no image
    [containerView addSubview:buttonLocation];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:containerView];
    
    self.navigationItem.rightBarButtonItem = barButtonItem;
}


// ACTION Button
- (void)actionButtonBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)actionButtonMenu{
    
}

- (void)actionButtonLocation{
    sharedAppDelegate.isLocated = NO;
    [self initLocationManager];
}

@end
