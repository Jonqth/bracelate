//
//  FZQueries.h
//  bracelateapp
//
//  Created by Aymeric Gallissot on 03/02/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FZQueries : NSObject

@property (strong, nonatomic) NSString *departune; // GEOLOC
@property (strong, nonatomic) NSString *arrival;
@property (strong, nonatomic) NSDate *date;

+ (FZQueries *)sharedQueries;

@end
